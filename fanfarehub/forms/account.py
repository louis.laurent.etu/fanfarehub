from django import forms
from django.contrib.auth import forms as auth_forms
from django.utils.translation import gettext_lazy as _

from tapeforms.contrib.bootstrap import Bootstrap4TapeformMixin

from ..models import User


class PlaceholderFormMixin(Bootstrap4TapeformMixin):
    field_label_css_class = 'sr-only'

    def apply_widget_options(self, field_name):
        field = self.fields[field_name]
        # add a placeholder attribute with the label
        field.widget.attrs['placeholder'] = field.label


class AuthenticationForm(PlaceholderFormMixin, auth_forms.AuthenticationForm):
    username = forms.EmailField(
        label=_("Email address"),
        widget=forms.EmailInput(attrs={'autofocus': True}),
    )

    error_messages = {
        'invalid_login': _("Your email address and password didn't match."),
        'inactive': _(
            "Your account is inactive. You should contact an administrator if "
            "you didn't receive the email with the activation instructions."
        ),
    }


class PasswordResetForm(PlaceholderFormMixin, auth_forms.PasswordResetForm):
    email = forms.EmailField(
        label=_("Email address"),
        max_length=254,
        widget=forms.EmailInput(attrs={'autocomplete': 'email'}),
    )


class SetPasswordForm(Bootstrap4TapeformMixin, auth_forms.SetPasswordForm):
    pass


# REGISTRATION
# ------------------------------------------------------------------------------


class RegisterForm(Bootstrap4TapeformMixin, auth_forms.UserCreationForm):
    email = forms.EmailField(
        label=_("Email address"),
        max_length=254,
        widget=forms.EmailInput(attrs={'autocomplete': 'email'}),
    )

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2', 'first_name', 'last_name')


# CURRENT USER
# ------------------------------------------------------------------------------


class PasswordChangeForm(
    Bootstrap4TapeformMixin, auth_forms.PasswordChangeForm
):
    pass


class ProfileUpdateForm(Bootstrap4TapeformMixin, forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name']
