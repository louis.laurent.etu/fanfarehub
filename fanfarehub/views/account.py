from django.conf import settings
from django.contrib import messages
from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView

from django_registration.backends.activation.views import (
    ActivationView,
    RegistrationView,
)

from ..forms.account import (
    AuthenticationForm,
    PasswordChangeForm,
    PasswordResetForm,
    ProfileUpdateForm,
    RegisterForm,
    SetPasswordForm,
)


class LoginView(auth_views.LoginView):
    """
    Logs a user in or link to registration and password reset.
    """

    authentication_form = AuthenticationForm
    template_name = 'account/login.html'
    extra_context = {'title': _("Sign in")}


class LogoutView(auth_views.LogoutView):
    """
    Logs the user out.
    """

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        messages.success(request, gettext("You have been logged out."))
        return response


class PasswordResetView(auth_views.PasswordResetView):
    """
    Allows a user to reset their password by generating and emailing a one-time
    use link.
    """

    form_class = PasswordResetForm
    success_url = settings.LOGIN_URL
    template_name = 'account/password/reset.html'
    email_template_name = 'account/password/reset_email.txt'
    subject_template_name = 'account/password/reset_email_subject.txt'
    title = _("Password reset")

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('account:password_change')
        return super().get(*args, **kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.info(
            self.request,
            gettext(
                "An email has been sent to the address you entered, if it "
                "is attached to a valid account. It contains instructions "
                "for setting your password."
            ),
        )
        return response


class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    """
    Confirms the password reset with a form for entering a new one.
    """

    form_class = SetPasswordForm
    success_url = settings.LOGIN_URL
    template_name = 'account/password/reset_confirm.html'
    title = _("Password reset")

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(
            self.request, gettext("Your password has been changed.")
        )
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # force the title on the error page
        context['title'] = self.title
        return context


# REGISTRATION
# ------------------------------------------------------------------------------


class RegisterView(RegistrationView):
    form_class = RegisterForm
    template_name = 'account/register.html'
    email_body_template = 'account/activation_email.txt'
    email_subject_template = 'account/activation_email_subject.txt'
    success_url = settings.LOGIN_URL
    disallowed_url = reverse_lazy('account:registration_closed')
    extra_context = {'title': _("Register an account")}

    def get_success_url(self, user=None):
        messages.success(
            self.request,
            gettext(
                "Your account has been created but you have to activate it "
                "in order to log in. An email with the activation instructions "
                "has been sent to you."
            ),
        )
        return super().get_success_url(user)


class RegistrationClosedView(TemplateView):
    template_name = 'account/registration_closed.html'
    extra_context = {'title': _("Register an account")}
    register_url = reverse_lazy('account:register')

    def get(self, request):
        if settings.REGISTRATION_OPEN:
            return HttpResponseRedirect(self.register_url)
        return super().get(request)


class ActivateView(ActivationView):
    template_name = 'account/activation_failed.html'
    success_url = settings.LOGIN_URL
    extra_context = {'title': _("Activation failed")}

    def get_success_url(self, user=None):
        messages.success(
            self.request,
            gettext("Your account has been activated, you can now log in."),
        )
        return super().get_success_url(user)


# CURRENT USER
# ------------------------------------------------------------------------------


class PasswordChangeView(auth_views.PasswordChangeView):
    """
    Changes the password of the currently logged in user.
    """

    form_class = PasswordChangeForm
    template_name = 'account/password/change.html'
    success_url = reverse_lazy('account:profile_update')
    extra_context = {'title': _("Change my password")}


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    """
    Updates the profile of the currently logged in user.
    """

    form_class = ProfileUpdateForm
    template_name = 'account/profile_update.html'
    extra_context = {'title': _("My profile")}

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        return self.request.path
