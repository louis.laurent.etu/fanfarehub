import factory
from factory.django import DjangoModelFactory
from factory.faker import Faker

from .. import models


class StandFactory(DjangoModelFactory):
    name = factory.Sequence(lambda x: 'Stand #{}'.format(x))

    class Meta:
        model = models.Stand


class InstrumentFactory(DjangoModelFactory):
    name = factory.Sequence(lambda x: 'Instrument #{}'.format(x))
    stand = factory.SubFactory(StandFactory)

    class Meta:
        model = models.Instrument


class UserFactory(DjangoModelFactory):
    first_name = Faker('first_name')
    last_name = Faker('last_name')
    email = factory.Sequence(lambda x: 'user{0}@example.org'.format(x))

    class Meta:
        model = models.User

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # override the default method to use a custom method from the manager
        # which is required to set the user's password properly
        return cls._get_manager(model_class).create_user(*args, **kwargs)
