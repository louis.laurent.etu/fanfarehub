import pytest

from .factories import UserFactory


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


@pytest.fixture
def test_user():
    return UserFactory(email='user@example.org', password='password')
