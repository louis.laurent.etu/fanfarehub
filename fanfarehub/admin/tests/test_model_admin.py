from django.urls import reverse

from pytest_django.asserts import assertRedirects

from fanfarehub.models import Stand

from .utils import AdminViewMixin

LOGIN_URL = reverse('account:login')


class TestLogin(AdminViewMixin):
    url = reverse('admin:login')

    def test_redirect_to_login(self):
        assertRedirects(
            self.get(user=None),
            '{}?next={}'.format(LOGIN_URL, reverse('admin:index')),
        )

    def test_redirect_to_index(self):
        assertRedirects(self.get(), reverse('admin:index'))


class TestStandAdmin(AdminViewMixin):
    def test_add(self):
        url = reverse('admin:fanfarehub_stand_add')

        form = self.get(url, status=200).forms[1]
        form['name'] = 'Saxophone'
        form['instruments-0-name'] = 'Saxophone Alto'
        form['instruments-0-pitch'] = 'Eb'
        response = form.submit(status=302).follow()

        table = response.html.select_one('table#result_list')
        assert len(table.select('tbody tr')) == 1

        assert Stand.objects.get(name='Saxophone').instruments.count() == 1


class TestUserAdmin(AdminViewMixin):
    def test_add(self):
        url = reverse('admin:fanfarehub_user_add')

        form = self.get(url, status=200).forms[1]
        assert len(form.fields) == 9
        assert 'profile-TOTAL_FORMS' not in form.fields

        form['email'] = 'camille@dupont.org'
        form['password1'] = 'SkaYiddish'
        form['password2'] = 'SkaYiddish'
        form['first_name'] = 'Camille'
        form['last_name'] = 'Dupont'
        response = form.submit(status=302).follow()

        form = response.forms[1]
        form['profile-0-pseudo'] = "Cam"
        response = form.submit(status=302).follow()

        table = response.html.select_one('table#result_list')
        assert len(table.select('tbody tr')) == 2
